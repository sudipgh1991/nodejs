const path = require('path');
const express = require('express');
const hbs = require('hbs');
const geocode = require('./utils/geocode');
const forecast = require('./utils/forecast');

const app = express();
const port = process.env.PORT || 3000;

//Define paths for Express config
const publicDirectoryPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../public/templates/views');
const partialsPath = path.join(__dirname, '../public/templates/partials');

//Setup handlebars engine and views location
app.set('view engine', 'hbs');
app.set('views', viewsPath); //Customize the viewsPath for hbs
hbs.registerPartials(partialsPath);

//Setup static directory to serve
app.use(express.static(publicDirectoryPath));

app.get('', (req, res) => {
  res.render('index', {
    title: 'Weather',
    name: 'Sudip Ghosh',
  });
});

app.get('/weather', (req, res) => {
  if (!req.query.address) {
    return res.send({
      error: 'Address needs to be provided',
    });
  }
  geocode(req.query.address, (error, { latitude, longitude } = {}) => {
    if (error) {
      return res.send({
        error: error,
      });
    }
    forecast(latitude, longitude, (error, forecastData) => {
      if (error) {
        return res.send({
          error: error,
        });
      }
      res.send({
        address: req.query.address,
        latitude: latitude,
        longitude: longitude,
        forecast: forecastData,
      });
    });
  });
});

app.get('/products', (req, res) => {
  if (!req.query.search) {
    return res.send({
      error: 'You must provide a search term',
    });
  }
  console.log(req.query);
  res.send({
    products: [],
  });
});

app.get('/about', (req, res) => {
  res.render('about', {
    title: 'About',
    name: 'Sudip Ghosh',
  });
});

app.get('/help', (req, res) => {
  res.render('help', {
    message: 'This is a help message',
    title: 'Help',
    name: 'Sudip Ghosh',
  });
});

app.get('/help/*', (req, res) => {
  res.render('404', {
    title: 'Page under help not found',
    name: 'Sudip Ghosh',
  });
});

app.get('*', (req, res) => {
  res.render('404', {
    title: 'Page not found',
    name: 'Sudip Ghosh',
  });
});

//start the server
app.listen(port, () => {
  console.log('Server is up on port ' + port);
});
