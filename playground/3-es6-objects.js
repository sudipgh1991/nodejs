// Object property shorthand

const name = 'Sudip';
const userAge = 28;

const user = {
  name,
  age: userAge,
  location: 'Jamshedpur',
};
console.log(user);

//Object destructuring

const product = {
  label: 'Red Notebook',
  price: 3,
  stock: 201,
  salePrice: undefined,
  rating: 4.2,
  test: {
    nestedObj: 1,
  },
};

// const label = product.label;
// const stock = product.stock;

// const { label: productLabel, stock, rating = 5 } = product;
// console.log(productLabel);
// console.log(stock);
// console.log(rating);

const transaction = (type, { label, stock }) => {
  console.log(type, label, stock);
};
transaction('order', product);
