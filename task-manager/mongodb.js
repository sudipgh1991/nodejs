//CRUD
// const mongodb = require('mongodb');
// const MongoClient = mongodb.MongoClient;
// const ObjectID = mongodb.ObjectID

const { MongoClient, ObjectID } = require('mongodb');

const connectionURL = 'mongodb://127.0.0.1:27017';
const databaseName = 'task-manager';

// const id = new ObjectID();
// console.log(id);
// console.log(id.getTimestamp());

MongoClient.connect(
  connectionURL,
  { useNewUrlParser: true },
  (error, client) => {
    if (error) {
      return console.log('Unable to connect to database');
    }
    const db = client.db(databaseName);

    // CREATE
    // db.collection('users').insertOne(
    //   {
    //     name: 'Vishal',
    //     age: 25,
    //   },
    //   (error, result) => {
    //     if (error) {
    //       return console.log('Unable to insert user');
    //     }
    //     console.log(result.ops);
    //   }
    // );

    // db.collection('users').insertMany(
    //   [
    //     {
    //       name: 'Krish',
    //       age: 25,
    //     },
    //     {
    //       name: 'Tamal',
    //       age: 26,
    //     },
    //   ],
    //   (error, result) => {
    //     if (error) {
    //       return console.log('Unable to insert users');
    //     }
    //     console.log(result.ops);
    //   }
    // );

    // db.collection('tasks').insertMany(
    //   [
    //     {
    //       description: 'Buy Grocery',
    //       completed: true,
    //     },
    //     {
    //       description: 'Do Homework',
    //       completed: true,
    //     },
    //     {
    //       description: 'Cooking',
    //       completed: false,
    //     },
    //   ],
    //   (error, result) => {
    //     if (error) {
    //       return console.log('Unable to insert users');
    //     }
    //     console.log(result.ops);
    //   }
    // );

    //READ
    // db.collection('users').findOne({ name: 'Vishal' }, (error, user) => {
    //   if (error) {
    //     return console.log('Unable to fetch');
    //   }
    //   console.log(user);
    // });

    // db.collection('users')
    //   .find({ age: 25 })
    //   .toArray((error, users) => {
    //     if (error) {
    //       return console.log('Unable to fetch');
    //     }
    //     console.log(users);
    //   });

    // db.collection('users')
    //   .find({ age: 25 })
    //   .count((error, count) => {
    //     if (error) {
    //       return console.log('Unable to fetch');
    //     }
    //     console.log(count);
    //   });

    //homework
    // db.collection('tasks').findOne(
    //   { _id: new ObjectID('5eca877aae649d4aa4be1c14') },
    //   (error, task) => {
    //     if (error) {
    //       return console.log('Unable to fetch');
    //     }
    //     console.log(task);
    //   }
    // );

    // db.collection('tasks')
    //   .find({ completed: true })
    //   .toArray((error, tasks) => {
    //     if (error) {
    //       return console.log('Unable to fetch');
    //     }
    //     console.log(tasks);
    //   });

    //UPDATE
    // db.collection('users')
    //   .updateOne(
    //     { _id: new ObjectID('5eca83fd3c108b24f4be727a') },
    //     {
    //       // $set: {
    //       //   name: 'Avinash',
    //       // },
    //       $inc: {
    //         age: 1,
    //       },
    //     }
    //   )
    //   .then((result) => {
    //     console.log(result);
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });

    //homework
    // db.collection('tasks')
    //   .updateMany(
    //     {
    //       completed: false,
    //     },
    //     {
    //       $set: {
    //         completed: true,
    //       },
    //     }
    //   )
    //   .then((result) => {
    //     console.log(result.modifiedCount);
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });

    //DELETE
    // db.collection('users')
    //   .deleteMany({
    //     age: 27,
    //   })
    //   .then((result) => {
    //     console.log(result);
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });

    //homework
    db.collection('tasks')
      .deleteOne({
        description: 'Cooking',
      })
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        console.log(error);
      });
  }
);
